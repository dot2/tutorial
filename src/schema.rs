table! {
    tutorials (id) {
        id -> Varchar,
        pid -> Nullable<Varchar>,
        tid -> Varchar,
        title -> Varchar,
        rank -> Nullable<Integer>,
        description -> Nullable<Varchar>,
        keyword -> Nullable<Varchar>,
        content -> Text,
        hidden -> Nullable<Bool>,
        created_at -> Nullable<Timestamp>,
        updated_at -> Nullable<Timestamp>,
    }
}

table! {
    types (id) {
        id -> Varchar,
        pid -> Nullable<Varchar>,
        name -> Varchar,
        rank -> Nullable<Integer>,
        description -> Nullable<Varchar>,
        keyword -> Nullable<Varchar>,
        icon -> Nullable<Varchar>,
        pic -> Nullable<Varchar>,
        has_tutorial -> Nullable<Bool>,
        hidden -> Nullable<Bool>,
        created_at -> Nullable<Timestamp>,
    }
}

table! {
    users (id) {
        id -> Char,
        username -> Varchar,
        password -> Varchar,
        avatar -> Nullable<Varchar>,
        enable -> Nullable<Bool>,
        created_at -> Nullable<Timestamp>,
        updated_at -> Nullable<Timestamp>,
    }
}

joinable!(tutorials -> types (tid));

allow_tables_to_appear_in_same_query!(
    tutorials,
    types,
    users,
);
