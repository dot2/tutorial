use std::str;
use uuid::Uuid;
use std::{env, fs};
pub mod errors;

pub fn uuid_str() -> String {
    str::replace(format!("{}", Uuid::new_v4()).as_str(), "-", "")
}

pub fn md5(password: String) -> String {
    password
}

// 检测路径是否存在，支持多级目录
pub fn dir_exists(path: &str) -> bool {
    let mut p = env::current_dir().unwrap();
    p.push(path);
    match fs::metadata(p) {
      Err(_) => false,
      Ok(metadata) => metadata.is_dir(),
    }
  }
  